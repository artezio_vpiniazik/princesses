﻿using Disney_Princess.DataManager;
using Disney_Princess.Models;
using Disney_Princess.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disney_Princess.Logic
{    
    class CmdListener 
    {             
        public static bool IsExit = false;
        public static List<Princess> WorkPrincesses;
        private IDataManager datamanager;
        private DisneyParser Parse;
        private IActionGiver actgiver;
        public CmdListener(IDataManager datamanager,DisneyParser Parse,IActionGiver actgiver)
        {
            this.actgiver = actgiver;
            this.datamanager = datamanager;
            this.Parse = Parse;
            WorkPrincesses = datamanager.LoadPrincess();
            ListenCommand();
        }               
        public void ExecuteCommand(Command command)
        {
            Action act=actgiver.GiveCommand(command);
            act.Invoke(command);      
        }
        private void ListenCommand()
        {
            do
            {                
                try
                {
                    ExecuteCommand(Parse.FromLineToCommand(Console.ReadLine()));
                }
                catch (FormatException e) { Console.WriteLine("Error:"+ e.Message); }                
            }
            while (!IsExit);
        }
    }
}
