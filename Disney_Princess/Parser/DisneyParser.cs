﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Disney_Princess.Models;

namespace Disney_Princess.Parser
{
    class DisneyParser 
    {
        private int MaxNumberOfCommand=6;
        private string[] GetCutedLine(string line, bool ToPrincess)
        {
            if (!ToPrincess)
            {
                string[] result=line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var word in result)
                {
                    word.Trim();
                }
                return result;
            }
            else
            {
                line = line.Replace(" ", string.Empty);
                return line.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }
        private void ValidateCommand(string[] CommandParams)
        {
            if (CommandParams.Length >MaxNumberOfCommand )
            {
                throw new FormatException("Too long command");
            }
            else
            {
                throw new FormatException("Too short command");
            }
        }
        public Command FromLineToCommand(string line)
        {
            string[] CommandParams = GetCutedLine(line, false);            
            switch (CommandParams.Length)
            {
                case 6:
                    return new Command
                    {
                        CommandName = CommandParams[0],
                        princess = new Princess
                        {
                            Number = Convert.ToInt32(CommandParams[1]),
                            Name = CommandParams[2],
                            Age = Convert.ToInt32(CommandParams[3]),
                            HairColor = CommandParams[4],
                            EyeColor = CommandParams[5]
                        }
                    };
                case 1:
                    return new Command
                    {
                        CommandName = CommandParams[0]
                    };
                case 2:
                    return new Command
                    {
                        CommandName = CommandParams[0],
                        princess=new Princess
                        {
                            Number=Convert.ToInt32(CommandParams[1])
                        }
                    };
                default:
                    ValidateCommand(CommandParams);
                    throw new FormatException("Something went wrong");
            }
        }
        public Princess FromLineToPrincess(string line)
        {
            string[] PrincessParams = GetCutedLine(line, true);
            return new Princess
            {
                Number = Convert.ToInt32(PrincessParams[0]),
                Name = PrincessParams[1],
                Age = Convert.ToInt32(PrincessParams[2]),
                HairColor = PrincessParams[3],
                EyeColor = PrincessParams[4]
            };
        }
    }
}
