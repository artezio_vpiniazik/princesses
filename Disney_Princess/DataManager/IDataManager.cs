﻿using Disney_Princess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disney_Princess.DataManager
{
    interface IDataManager
    {
        List<Princess> LoadPrincess();
    }
}
